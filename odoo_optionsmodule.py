#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import xmlrpclib

options  = dict(
    install='button_immediate_install',
    upgrade='button_immediate_upgrade',
    uninstall='button_immediate_uninstall',
)

# Log in
args = dict(
    url='http://localhost:8069',
    database='database_odoo',
    user='admin',
    password='admin',
    module='res.partner',
    option='install', # upgrade, uninstall
)


ws_common = xmlrpclib.ServerProxy(args['url'] + '/xmlrpc/common')
uid = ws_common.login(args['database'], args['user'], args['password'])
print "Logged in to the common web service.", "uid", uid
# Get the object proxy
ws_object = xmlrpclib.ServerProxy(args['url'] + '/xmlrpc/object')
print "Connected to the object web service."

# Find the parent location by name
res_ids = ws_object.execute(
    args['database'], uid, args['password'],
    'ir.module.module', 'search', [('name', '=', args['module'])])
if len(res_ids) != 1:
    raise Exception("Search failed")

# Execute option in the module
print "%sing '%s'" % (args['option'].capitalize(), args['module'])
ws_object.execute(
    args['database'], uid, args['password'],
    'ir.module.module', options[args['option']], res_ids)

print "All done."
